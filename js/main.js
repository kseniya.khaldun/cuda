(function () {
  const searchBox = document.createElement('div');
  searchBox.attachShadow({ mode: 'open' });
  document.body.insertAdjacentElement('afterbegin', searchBox);

  searchBox.shadowRoot.innerHTML = `
<div class="box">
  <div class="box__header">
    <h1 class="box__title">Search node element</h1>
    <button class="button-close">X</button>
  </div>

  <div class="form">
    <label>
      <input type="text" class="form__input" value="" />
    </label>
    <button class="button--search">Search</button>
  </div>

  <div class="buttons">
    <button disabled class="button--prev">Prev</button>
    <button disabled class="button--next">Next</button>
    <button disabled class="button--parent">Parent</button>
    <button disabled  class="button--children">Children</button>
  </div>
</div>

<style>
  .box {
    z-index: 9999;
    position: fixed;
    background-color: #000000;
    padding: 20px;
    margin: 20px;
    color: #ffffff;
    width: 300px
  }

  .box__header {
    display: flex;
    justify-content: space-between;
  }

  .box__title {
    font-size: 20px;
  }

  .button-close {
    align-self: flex-start;
    border: none;
    background-color: none
    color: #fffff;
    cursor: pointer;
    width: 20px;
  }

  button {
    cursor:pointer;
    width: 70px;
  }

  .form {
    display: flex;
    justify-content: space-between;
  }

  .buttons {
    display: flex;
    justify-content: space-between;
    margin-top:20px;
  }

</style>
`

  const box = searchBox.shadowRoot.querySelector(".box");
  const input = searchBox.shadowRoot.querySelector(".form__input");
  const prevBtn = searchBox.shadowRoot.querySelector(".button--prev");
  const nextBtn = searchBox.shadowRoot.querySelector(".button--next");
  const parentBtn = searchBox.shadowRoot.querySelector(".button--parent");
  const childrenBtn = searchBox.shadowRoot.querySelector(".button--children");
  let searchingElem

  box.addEventListener("click", (event) => {

    switch (event.target.className) {
      case "button--search":
        onSearchClick();
        break
      case "button--prev":
        updateSearchingElement(searchingElem.previousElementSibling)
        break
      case "button--parent":
        updateSearchingElement(searchingElem.parentElement)
        break
      case "button--next":
        updateSearchingElement(searchingElem.nextElementSibling)
        break
      case "button--children":
        updateSearchingElement(searchingElem.firstElementChild)
        break
      case "button-close":
        box.remove();
        Object.assign(searchingElem.style, { border: "none" });
        break
    }
  })

  function onSearchClick() {
    const searchingNode = document.querySelector(input.value)
    updateSearchingElement(searchingNode)
  }

  function changeButtonStatus(buttons = [], status = true) {
    buttons.forEach(button => button.disabled = status)
  }

  function updateSearchingElement(newElem) {
    if (searchingElem) {
      Object.assign(searchingElem.style, { border: "none" })
    }

    searchingElem = newElem;
    
    Object.assign(searchingElem.style, { border: "1px solid red" })

    const buttonsChangeStatus = []

    if (!searchingElem.parentElement) {
      buttonsChangeStatus.push(parentBtn)
    }
    if (!searchingElem.nextElementSibling) {
      buttonsChangeStatus.push(nextBtn)
    }
    if (!searchingElem.previousElementSibling) {
      buttonsChangeStatus.push(prevBtn)
    }
    if (!searchingElem.firstElementChild) {
      buttonsChangeStatus.push(childrenBtn)
    }
    changeButtonStatus([parentBtn, nextBtn, prevBtn, childrenBtn], false)
    changeButtonStatus(buttonsChangeStatus, true)
  }


  function dragElement(element) {
    element.onmousedown = function (event) {
      let shiftX = event.clientX - element.getBoundingClientRect().left;
      let shiftY = event.clientY - element.getBoundingClientRect().top;

      function moveAt(clientX, clientY) {
        box.style.left = clientX - shiftX + "px";
        box.style.top = clientY - shiftY + "px";
      }

      function onMouseMove(event) {
        moveAt(event.clientX, event.clientY)
      };

      document.onmousemove = onMouseMove;
      element.onmouseup = function () {
        document.onmousemove = null;
        document.onmouseup = null;
      };
      element.ondragstart = function () {
        return false;
      };
    }
  }

  dragElement(box);

}())

/*javascript:void%20function(){function%20a(){const%20a=document.querySelector(g.value);c(a)}function%20b(a=[],b=!0){a.forEach(a=%3Ea.disabled=b)}function%20c(a){l%26%26Object.assign(l.style,{border:%22none%22}),l=a,console.log(l),Object.assign(l.style,{border:%221px%20solid%20red%22});const%20c=[];l.parentElement||c.push(j),l.nextElementSibling||c.push(i),l.previousElementSibling||c.push(h),l.firstElementChild||c.push(k),b([j,i,h,k],!1),b(c,!0)}const%20d=document.createElement(%22div%22);d.attachShadow({mode:%22open%22}),document.body.insertAdjacentElement(%22afterbegin%22,d),d.shadowRoot.innerHTML=`
%3Cdiv%20class=%22box%22%3E
%20%20%3Cdiv%20class=%22box__header%22%3E
%20%20%20%20%3Ch1%20class=%22box__title%22%3ESearch%20node%20element%3C/h1%3E
%20%20%20%20%3Cbutton%20class=%22button-close%22%3EX%3C/button%3E
%20%20%3C/div%3E

%20%20%3Cdiv%20class=%22form%22%3E
%20%20%20%20%3Clabel%3E
%20%20%20%20%20%20%3Cinput%20type=%22text%22%20class=%22form__input%22%20value=%22%22%20/%3E
%20%20%20%20%3C/label%3E
%20%20%20%20%3Cbutton%20class=%22button--search%22%3ESearch%3C/button%3E
%20%20%3C/div%3E

%20%20%3Cdiv%20class=%22buttons%22%3E
%20%20%20%20%3Cbutton%20disabled%20class=%22%20button--prev%22%3EPrev%3C/button%3E
%20%20%20%20%3Cbutton%20disabled%20class=%22%20button--next%22%3ENext%3C/button%3E
%20%20%20%20%3Cbutton%20disabled%20class=%22%20button--parent%22%3EParent%3C/button%3E
%20%20%20%20%3Cbutton%20disabled%20%20class=%22%20button--children%22%3EChildren%3C/button%3E
%20%20%3C/div%3E
%3C/div%3E

%3Cstyle%3E
%20%20.box%20{
%20%20%20%20z-index:%209999;
%20%20%20%20position:%20fixed;
%20%20%20%20background-color:%20%23000000;
%20%20%20%20padding:%2020px;
%20%20%20%20margin:%2020px;
%20%20%20%20color:%20%23ffffff;
%20%20%20%20width:%20300px
%20%20}

%20%20.box__header%20{
%20%20%20%20display:%20flex;
%20%20%20%20justify-content:%20space-between;
%20%20}

%20%20.box__title%20{
%20%20%20%20font-size:%2020px;
%20%20}

%20%20.button-close%20{
%20%20%20%20align-self:%20flex-start;
%20%20%20%20border:%20none;
%20%20%20%20background-color:%20none
%20%20%20%20color:%20%23fffff;
%20%20%20%20cursor:%20pointer;
%20%20%20%20width:%2020px;
%20%20}

%20%20button%20{
%20%20%20%20cursor:pointer;
%20%20%20%20width:%2070px;
%20%20}

%20%20.form%20{
%20%20%20%20display:%20flex;
%20%20%20%20justify-content:%20space-between;
%20%20}

%20%20.buttons%20{
%20%20%20%20display:%20flex;
%20%20%20%20justify-content:%20space-between;
%20%20%20%20margin-top:20px;
%20%20}

%3C/style%3E
`;const%20e=d.shadowRoot.querySelector(%22button-close%22),f=d.shadowRoot.querySelector(%22.box%22),g=d.shadowRoot.querySelector(%22.form__input%22),h=d.shadowRoot.querySelector(%22.button--prev%22),i=d.shadowRoot.querySelector(%22.button--next%22),j=d.shadowRoot.querySelector(%22.button--parent%22),k=d.shadowRoot.querySelector(%22.button--children%22);let%20l;f.addEventListener(%22click%22,b=%3E{switch(b.target.className){case%22button--search%22:a();break;case%22button--prev%22:c(l.previousElementSibling);break;case%22button--parent%22:c(l.parentElement);break;case%22button--next%22:c(l.nextElementSibling);break;case%22button--children%22:c(l.firstElementChild);break;case%22button-close%22:f.remove();}}),function(a){a.onmousedown=function(b){function%20c(a,b){f.style.left=a-d+%22px%22,f.style.top=b-e+%22px%22}let%20d=b.clientX-a.getBoundingClientRect().left,e=b.clientY-a.getBoundingClientRect().top;document.onmousemove=function(a){c(a.clientX,a.clientY)},a.onmouseup=function(){document.onmousemove=null,document.onmouseup=null},a.ondragstart=function(){return!1}}}(f)}();*/